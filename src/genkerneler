#!/usr/bin/env bash

# A simple Gentoo script to
# generate the kernel and
# reconfigure the system
# using sys-kernel/genkernel
# or sys-kernel/genkernel-next

trap 'exit 128' INT
export PATH

cores=$(nproc) || cores=2

bold_msg() {
    echo "$(tput bold)$1$(tput sgr0)"
}

err_msg() {
    bold_msg "$1"
    exit 1
}

# Check root

if [ "$EUID" -ne 0 ]; then
    err_msg "This script must be run with root privileges!"
    exit 1
fi

# Select the kernel

eselect kernel list
bold_msg "Which kernel will be used?"

# Set the kernel

read -r kvar
eselect kernel set "$kvar" || err_msg "Couldn't select the kernel!"

# Go to the kernel sources

cd /usr/src/linux || err_msg "Couldn't cd to linux sources directory!"
bold_msg "$(pwd)"
bold_msg "Remember to save the configuration as '.config'!"
sleep 3

# Compile the kernel

zcat /proc/config.gz > /usr/src/linux/.config
genkernel --menuconfig --no-mrproper --kernel-ld=ld.bfd --makeopts=-j$cores --install all || err_msg "There was an genkernel error!"
bold_msg "Kernel compiled..."

# Rebuild the modules

emerge @module-rebuild || err_msg "Couldn't rebuild some packages!"
bold_msg "Modules rebuilt..."

# Generate grub config file

grub-mkconfig -o /boot/grub/grub.cfg || err_msg "Couldn't generate grub.cfg!"
bold_msg "Grub Bootloader updated..."

# Finish

bold_msg "OK"
bold_msg "Genkerneler has done its job!"
